use serde::{Serialize, Deserialize};
use crate::modules::Id;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum TypeQuest {
    MVP,
    DS,
    ML,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum LevelQuest {
    BEGINNER,
    EXPERT,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Quest {
    pub id: Option<Id>,
    pub name: String,
    pub type_quest: TypeQuest,
    pub level: LevelQuest,
    pub description: String,
    pub info: String,
}

pub mod repository;
pub mod controller;
