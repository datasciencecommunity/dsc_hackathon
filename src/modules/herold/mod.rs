use serde::{Serialize, Deserialize};
use crate::modules::Id;
use chrono::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Herold {
    pub id: Option<Id>,
    pub data: String,
    pub created_at: DateTime<Utc>,
}

pub mod repository;
pub mod controller;

