use crate::modules::Id;
use actix_web::{HttpResponse, Responder, get, post, patch, delete, web};
use mongodb::{Client};
use std::sync::Mutex;
use crate::herold::repository::HeroldRepository;

#[get("/")]
async fn info(data: web::Data<Mutex<Client>>) -> impl Responder {
    match HeroldRepository::info(data.into_inner()).await {
        Some(result) => HttpResponse::Ok().json(result),
        None => HttpResponse::NoContent().body("")
    }
}
