use crate::modules::Id;
use mongodb::{Client, options::FindOneOptions};
use std::sync::{Mutex, Arc};
use futures::stream::StreamExt;
use bson::{doc};
use crate::hacker::Hacker;

static DB_NAME: &'static str = "dsc_hackathon";
static COLLECTION_NAME: &'static str = "herolds";

pub struct HeroldRepository {}

impl HeroldRepository {
    pub async fn info(data: Arc<Mutex<Client>>) -> Option<bson::Document> {
        let collection = data
            .lock()
            .unwrap()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let find_option = FindOneOptions::builder().sort(doc! { "created_at": -1}).build();
        let cursor = collection.find_one(doc!{}, find_option).await.unwrap();
        match cursor {
            Some(doc) => Some(doc),
            _ => None
        }
    }
}

