use serde::{Serialize, Deserialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Id {
    pub value: Uuid,
}

impl Id {
    pub fn new() -> Id {
        Id {
            value: Uuid::new_v4()
        }
    }

    pub fn from_string(id: String) -> Id {
        Id {
            value: Uuid::parse_str(id.as_str()).unwrap()
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Herold {
    id: Id,
    info: String,
}

pub mod hackathon;
pub mod hacker;
pub mod team;
pub mod quest;
pub mod quiz;
pub mod agenda;
pub mod herold;
