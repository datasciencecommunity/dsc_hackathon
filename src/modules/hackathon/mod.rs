use serde::{Serialize, Deserialize};
use crate::modules::Id;
use bson::{doc};
use chrono::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Hackathon {
    pub id: Option<Id>,
    pub title: String,
    pub date: DateTime<Utc>, 
}

pub mod repository;
pub mod controller;

