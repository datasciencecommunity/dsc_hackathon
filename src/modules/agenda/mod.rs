use serde::{Serialize, Deserialize};
use chrono::prelude::*;
use crate::modules::Id;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Period {
    pub from: DateTime<Utc>,
    pub to: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AgendaItem {
    pub id: Option<Id>,
    pub period: Period,
    pub title: String,
    pub description: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Agenda {
    pub id: Option<Id>,
    pub items: Vec<AgendaItem>,        
}

pub mod repository;
pub mod controller;
