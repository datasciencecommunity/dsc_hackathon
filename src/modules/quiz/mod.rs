use serde::{Serialize, Deserialize};
use crate::modules::Id;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Quiz {
    pub id: Option<Id>,
    pub title: String,
    pub uri: String,
}

pub mod repository;
pub mod controller;

