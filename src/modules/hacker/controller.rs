use crate::modules::Id;
use actix_web::{HttpResponse, Responder, get, post, patch, delete, web};
use mongodb::{Client};
use std::sync::Mutex;
use crate::hacker::repository::HackerRepository;

#[get("/")]
async fn get_all(data: web::Data<Mutex<Client>>) -> impl Responder {
    match HackerRepository::read_all(data.into_inner()).await {
        Some(result) => HttpResponse::Ok().json(result),
        None => HttpResponse::NoContent().body("")
    }
}


#[get("/{id}")]
async fn get(data: web::Data<Mutex<Client>>, web::Path((id)): web::Path<(String)>) -> impl Responder {
    match HackerRepository::read(data.into_inner(), Id::from_string(id)).await {
        Some(result) => HttpResponse::Ok().json(result),
        None => HttpResponse::NoContent().body("")
    }
}


#[post("/")]
async fn post(data: web::Data<Mutex<Client>>, req_body: String) -> impl Responder {
    match HackerRepository::create(data.into_inner(), serde_json::from_str(req_body.as_str()).unwrap()).await {
        Some(result) => {
            let formatted = serde_json::to_string(&result).unwrap();
            HttpResponse::Ok().body(formatted)
        },
        None => HttpResponse::BadRequest().body("")
    }
}

#[patch("/{id}")]
async fn patch(data: web::Data<Mutex<Client>>, req_body: String, web::Path((id)): web::Path<(String)>) -> impl Responder {
    match HackerRepository::update(
        data.into_inner(), 
        serde_json::from_str(req_body.as_str()).unwrap(), 
        Id::from_string(id)).await {
        Some(result) => {
            let formatted = serde_json::to_string(&result).unwrap();
            HttpResponse::Ok().body(formatted)
        },
        None => HttpResponse::BadRequest().body("")
    }
}

#[delete("/{id}")]
async fn delete(data: web::Data<Mutex<Client>>, web::Path((id)): web::Path<(String)>) -> impl Responder {
    match HackerRepository::delete(
        data.into_inner(), 
        Id::from_string(id)).await {
        Some(_) => {
            HttpResponse::Ok().body("")
        },
        None => HttpResponse::BadRequest().body("")
    }
}

