use serde::{Serialize, Deserialize};
use crate::modules::Id;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Mail {
    pub content: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Hacker {
    pub id: Option<Id>,
    pub mail: Mail,
    pub nick: String,
    pub password: String,
}

pub mod repository;
pub mod controller;

