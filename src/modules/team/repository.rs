use crate::modules::Id;
use mongodb::{Client};
use std::sync::{Mutex, Arc};
use futures::stream::StreamExt;
use bson::{doc};
use crate::team::Team;

static DB_NAME: &'static str = "dsc_hackathon";
static COLLECTION_NAME: &'static str = "teams";

pub struct TeamRepository {}

impl TeamRepository {
    pub async fn read_all(data: Arc<Mutex<Client>>) -> Option<Vec<bson::Document>> {
        let collection = data
            .lock()
            .unwrap()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let mut cursor = collection.find(doc!{}, None).await.unwrap();
        let mut results = Vec::new();
        while let Some(result) = cursor.next().await {
            match result {
                Ok(document) => {
                    results.push(document);
                }
                _ => {
                    return None
                }
            }
        }
        Some(results)
    }

    pub async fn read(data: Arc<Mutex<Client>>, id: Id) -> Option<bson::Document> {
        let collection = data
            .lock()
            .unwrap()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let mut cursor = collection.find(doc!{ "id.value": id.value.to_string() }, None).await.unwrap();
        match cursor.next().await {
            Some(doc) => Some(doc.unwrap()),
            _ => None
        }
    }

    pub async fn create(data: Arc<Mutex<Client>>, team: Team) -> Option<Id> {
        let collection = data
            .lock()
            .unwrap()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let mut team_new = team.clone();
        team_new.id = Some(Id::new());
        let result = collection.insert_one(bson::to_document(&team_new).unwrap(), None).await.unwrap();
        Some(Id::new())
    }

    pub async fn update(data: Arc<Mutex<Client>>, team: Team, id: Id) -> Option<()> {
        let collection = data
            .lock()
            .unwrap()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let mut team_new = team.clone();
        team_new.id = Some(id.clone());
        collection.update_one(
            doc!{"id.value": id.value.to_string()}, 
            bson::to_document(&team).unwrap(), None).await.unwrap();
        Some(())
    }

    pub async fn delete(data: Arc<Mutex<Client>>, id: Id) -> Option<Id> {
        let collection = data
            .lock()
            .unwrap()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        collection.delete_one(doc!{ "id.value": id.value.to_string() }, None).await.unwrap();
        Some(Id::new())
    }
}


