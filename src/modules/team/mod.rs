use serde::{Serialize, Deserialize};
use crate::modules::Id;
use crate::modules::hacker;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Team {
    pub id: Option<Id>,
    pub hackers: Vec<hacker::Hacker>,
    pub name: String,
}

pub mod repository;
pub mod controller;
