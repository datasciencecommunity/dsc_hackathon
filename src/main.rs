use std::env;
use std::sync::*;
use dotenv::dotenv;
use actix_web::{HttpServer, 
    App, 
    HttpResponse, 
    Responder, 
    get,
    web
};
use mongodb::{Client, options::ClientOptions};
pub mod modules;
use crate::modules::hacker;
use crate::modules::hackathon;
use crate::modules::herold;
use crate::modules::quest;
use crate::modules::quiz;
use crate::modules::team;
use crate::modules::agenda;

#[get("/ping")]
async fn ping() -> impl Responder {
    HttpResponse::Ok().body("Ping server!")
}

fn get_env() -> String {
    dotenv().ok();
    match env::var("DB_CONNECTION") {
        Ok(val) => val,
        Err(_) => String::from("")
    }
}

async fn get_db_client() -> Client {
    let options = ClientOptions::parse("mongodb://localhost:27017").await.unwrap();
    return Client::with_options(options).unwrap();
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let client = web::Data::new(Mutex::new(get_db_client().await));
    HttpServer::new(move || {
        App::new()
            .app_data(client.clone())
            .service(ping)
            .service(
                web::scope("/hackers")
                .service(hacker::controller::get_all)
                .service(hacker::controller::post)
                .service(hacker::controller::get)
                .service(hacker::controller::patch)
                .service(hacker::controller::delete)
            )
            .service(
                web::scope("/hackathons")
                .service(hackathon::controller::get_all)
                .service(hackathon::controller::post)
                .service(hackathon::controller::get)
                .service(hackathon::controller::patch)
                .service(hackathon::controller::delete)
            )
            .service(
                web::scope("/herolds")
                .service(herold::controller::info)
            )
            .service(
                web::scope("/quests")
                .service(quest::controller::get_all)
                .service(quest::controller::post)
                .service(quest::controller::get)
                .service(quest::controller::patch)
                .service(quest::controller::delete)
            )
            .service(
                web::scope("/quizzes")
                .service(quiz::controller::get_all)
                .service(quiz::controller::post)
                .service(quiz::controller::get)
                .service(quiz::controller::patch)
                .service(quiz::controller::delete)
            )
            .service(
                web::scope("/teams")
                .service(team::controller::get_all)
                .service(team::controller::post)
                .service(team::controller::get)
                .service(team::controller::patch)
                .service(team::controller::delete)
            )
            .service(
                web::scope("/agenda")
                .service(agenda::controller::get_all)
                .service(agenda::controller::post)
                .service(agenda::controller::get)
                .service(agenda::controller::patch)
                .service(agenda::controller::delete)
            )
    })
    .bind("127.0.0.1:8082")?
    .run()
    .await
}
